package com.example.listviewper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.BreakIterator;
import java.util.UUID;

import Modelo.AlumnosDb;

public class AlumnoAlta extends AppCompatActivity {

    private Button btnGuardar, btnRegresar, btnEliminar, btnImagen;
    private ItemAlumno alumno;
    private EditText txtNombre, txtMatricula, txtGrado;
    private ImageView imgAlumno;
    private TextView lblUriImagen;
    private int posicion;

    private AlumnosDb alumnosDb;


    //Variables necesarias para la carga de imagenes
    private static final int REQUEST_PERMISSION = 2;
    private static final int REQUEST_CODE = 1;
    private Bitmap bitmap;


    //Aplicacion app;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumno_alta);

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);
        btnImagen = (Button) findViewById(R.id.btnImagen);
        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtGrado = (EditText) findViewById(R.id.txtGrado);
        imgAlumno = (ImageView) findViewById(R.id.imgAlumno);
        lblUriImagen = (TextView) findViewById(R.id.lblUriImagen);

        Bundle bundle = getIntent().getExtras();
        alumno = (ItemAlumno) bundle.getSerializable("alumno");
        this.posicion = bundle.getInt("posicion", posicion);

        if(posicion >= 0){
            this.txtMatricula.setText(alumno.getTextMatricula());
            this.txtNombre.setText(alumno.getTextNombre());
            this.txtGrado.setText(alumno.getCarrera());

            File imageFile = new File(getFilesDir(), alumno.getImageId() + ".jpg");

            this.imgAlumno.setImageURI(Uri.fromFile(imageFile));
            this.lblUriImagen.setText(alumno.getImageId());
        }

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(posicion >= 0){

                    AlertDialog.Builder mensaje = new AlertDialog.Builder(AlumnoAlta.this);
                    mensaje.setTitle("Alumno");
                    mensaje.setMessage(" ¿Desea eliminar al alumno ?");
                    mensaje.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("ID","ID: " + alumno.getId());
                            Aplicacion.alumnosDb.deleteAlumno(alumno.getId());
                            Aplicacion.alumnos.remove(posicion);
                            Aplicacion.adaptador.notifyItemRemoved(posicion);
                            Toast.makeText(getApplicationContext(), "Se elimino con exito ",Toast.LENGTH_SHORT).show();
                            setResult(Activity.RESULT_OK);
                            finish();
                            Aplicacion.adaptador.notifyDataSetChanged();
                        }
                    });

                    mensaje.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    mensaje.show();
                }
            }
        });


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(alumno == null){
                    //agregar un nuevo alumno

                    alumno = new ItemAlumno();
                    alumno.setCarrera(txtGrado.getText().toString());
                    alumno.setTextMatricula(txtMatricula.getText().toString());
                    alumno.setTextNombre(txtNombre.getText().toString());
                    alumno.setImageId(lblUriImagen.getText().toString());

                    if(validar()){
                        AlumnosDb alumnosDb = new AlumnosDb(getApplicationContext());
                        //Inserta en la tabla de alumnos
                        alumnosDb.insertAlumno(alumno);
                        Aplicacion.alumnos.add(alumno);
                        Toast.makeText(getApplicationContext(), "Se inserto un alumno ",Toast.LENGTH_SHORT).show();
                        setResult(Activity.RESULT_OK);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Falto capturar datos ", Toast.LENGTH_SHORT).show();
                        finish();

                    }
                }

                if(posicion >= 0){
                    alumno.setTextMatricula(txtMatricula.getText().toString());
                    alumno.setTextNombre(txtNombre.getText().toString());
                    alumno.setCarrera(txtGrado.getText().toString());
                    alumno.setImageId(lblUriImagen.getText().toString());

                    //Actualiza en la tabla alumnos
                    Aplicacion.alumnosDb.updateAlumno(alumno);

                    Aplicacion.alumnos.get(posicion).setTextMatricula(alumno.getTextMatricula());
                    Aplicacion.alumnos.get(posicion).setTextNombre(alumno.getTextNombre());
                    Aplicacion.alumnos.get(posicion).setCarrera(alumno.getCarrera());
                    Aplicacion.alumnos.get(posicion).setImageId(alumno.getImageId());

                    Toast.makeText(getApplicationContext(), "Se modifico con exito ",Toast.LENGTH_SHORT).show();

                    setResult(Activity.RESULT_OK);
                    finish();
                }

            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        //Funcion boton cargar imagenes
        btnImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen();
            }
        });

    }

    //Funcion para cargar imagen
    private void cargarImagen() {
        if(ContextCompat.checkSelfPermission(AlumnoAlta.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
         != PackageManager.PERMISSION_GRANTED){

            // Si el permiso no esta concedido se solitia al usuario
            ActivityCompat.requestPermissions(AlumnoAlta.this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);

        }else {
            // Si el permiso ya está concedido
            abrirGaleria();
        }
    }

    private void abrirGaleria() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_PERMISSION){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //El usuario concedio el permiso
                abrirGaleria();
            }else {
                //El usaurio denegó el permiso
                Toast.makeText(AlumnoAlta.this, "Permiso denegado", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null){
            //Obtengo la imagen
            Uri selectedImageUri = data.getData();

            //Generar un identificador único para la imagen
            String imageId = UUID.randomUUID().toString();

            lblUriImagen.setText(imageId);

            //Copiar el archivo de la URI  a una ubicación en el sistema de archivos
            File destinationFile = new File(getFilesDir(), imageId + ".jpg");
            try {
                copyImageFile(selectedImageUri, destinationFile);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            try{
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                imgAlumno.setImageBitmap(bitmap);
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    private void copyImageFile(Uri sourceUri, File destinationFile) throws IOException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = getContentResolver().openInputStream(sourceUri);
            outputStream = new FileOutputStream(destinationFile);
            byte[] buffer = new byte[2048];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    // Manejar cualquier excepción de cierre de InputStream
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    // Manejar cualquier excepción de cierre de OutputStream
                }
            }
        }
    }

    public File getDirectorioArchivos(){
        return getFilesDir();
    }



    //Funcion para validar
    private boolean validar(){
        Log.d("nombre","validar " + txtNombre.getText().toString());
        if (txtNombre.getText().toString().equals("")) return false;
        if (txtMatricula.getText().toString().equals("")) return false;
        if (txtGrado.getText().toString().equals("")) return false;
        if(lblUriImagen.getText().toString().equals("")) return false;
        return true;
    }
}
