package com.example.listviewper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.Transliterator;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.stream.Collectors;

import android.widget.*;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Locale;

import org.jetbrains.annotations.NotNull;

public class AdapterAlumno extends RecyclerView.Adapter<AdapterAlumno.ViewHolder> implements View.OnClickListener{
    //Varaibles necesarias
    private ArrayList<ItemAlumno> listaAlumno;
    private ArrayList<ItemAlumno> listaOri;

    private View.OnClickListener listener;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<ItemAlumno> originalList; // Lista original sin filtrar

    //Consturctor
    public AdapterAlumno(ArrayList<ItemAlumno> listaAlumno, Context context){
        this.listaAlumno = listaAlumno ;
        //this.listaAlumno.addAll(listaAlumno);
        this.context = context;
        //this.listaOri = listaAlumno;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public AdapterAlumno.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.itemalumno, null);
        view.setOnClickListener(this);  //Escucha el evento click
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterAlumno.ViewHolder holder, int position) {
        ItemAlumno alumno = this.listaAlumno.get(position);
        holder.txtMatricula.setText(alumno.getTextMatricula());
        holder.txtNombre.setText(alumno.getTextNombre());
        holder.txtCarrera.setText(alumno.getCarrera());

        File directorioArchivos = (context).getFilesDir();

        File imageFile = new File(directorioArchivos, alumno.getImageId() + ".jpg");


        holder.idImagen.setImageURI(Uri.fromFile(imageFile));
    }

    @Override
    public int getItemCount() {
        return this.listaAlumno.size();
    }

    @Override
    public void onClick(View v) {
        if(listener != null) {
            listener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener listener){this.listener = listener;}

    public class ViewHolder extends RecyclerView.ViewHolder{
        private LayoutInflater inflater;
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrera;
        private ImageView idImagen;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            txtNombre = (TextView) itemView.findViewById(R.id.lblNombre);
            txtMatricula = (TextView) itemView.findViewById(R.id.lblMatricula);
            txtCarrera = (TextView) itemView.findViewById(R.id.lblCarrera);

            idImagen = (ImageView) itemView.findViewById(R.id.imgAlumno);
        }
    }

    //Creación de filtro
    public Filter getFilter(){
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                ArrayList<ItemAlumno> listaFiltrada = new ArrayList<>();

                if(constraint == null || constraint.length() == 0){
                    listaFiltrada.addAll(listaAlumno);
                    filterResults.values = listaFiltrada;
                    filterResults.count = listaFiltrada.size();

                }else {
                    String searchStr = constraint.toString().toLowerCase().trim();

                    for (ItemAlumno itemAlumno : listaAlumno) {

                        if (itemAlumno.getTextNombre().toLowerCase().contains(searchStr)) {
                            listaFiltrada.add(itemAlumno);
                        }
                    }
                    filterResults.values = listaFiltrada;
                    filterResults.count = listaFiltrada.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listaAlumno.clear();
                if (results.count > 0){
                    listaAlumno.addAll((ArrayList<ItemAlumno>) results.values);
                }
                notifyDataSetChanged();
            }
        };
    }
}
