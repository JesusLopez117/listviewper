package com.example.listviewper;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.AlumnosDb;

public class Aplicacion extends Application {
    //Acceder a objetos globales de la aplicación. Compartir objetos entre componentes.

    static ArrayList<ItemAlumno> alumnos;
    public static AdapterAlumno adaptador;

    public ArrayList<ItemAlumno> getAlumnos(){ return alumnos; }

    public AdapterAlumno getAdaptador(){ return adaptador; }

    static AlumnosDb alumnosDb;

    @Override
    public void onCreate(){
        super.onCreate();
        alumnosDb = new AlumnosDb(getApplicationContext());
        //alumnos = ItemAlumno.llenarAlumnos();
        alumnos = alumnosDb.allAlumnos();
        alumnosDb.openDataBase();

        Log.d("","OnCreate: Tamañi array list: " + this.alumnos.size());
        this.adaptador = new AdapterAlumno(this.alumnos, this);
    }

}
