package com.example.listviewper;

import android.content.Context;
import android.graphics.Bitmap;

import org.json.*;
import java.io.*;


import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ItemAlumno implements Serializable{
    //Varias extras

    static Context context;

    //Atributos de la clase
    private int id;
    private String carrera;
    private String textNombre;
    private String textMatricula;
    private String imageId;



    //Constructor
    public ItemAlumno() {
        this.carrera = "";
        this.textNombre = "";
        this.textMatricula = "";
        this.imageId = "";
    }

    public ItemAlumno(int id,String carrera, String nombre, String imageId, String matricula){
        this.id = id;
        this.carrera = carrera;
        this.textNombre = nombre;
        this.imageId = imageId;
        this.textMatricula = matricula;
    }

    //Encapsulamiento
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
    public String getTextNombre() {
        return textNombre;
    }

    public void setTextNombre(String textNombre) {
        this.textNombre = textNombre;
    }

    public String getTextMatricula() {
        return textMatricula;
    }

    public void setTextMatricula(String textMatricula) {
        this.textMatricula = textMatricula;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

}
